from src.components.component import Component
from dash import html, dcc

class Window(Component):

    def build(self):
        return self._layout()
    
    def _layout(self):
        return html.Div(children=
            [
                html.H6("Change the value in the text box to see callbacks in action!"),
                html.Div(children=
                    [
                        "Input: ",
                        dcc.Input(
                            id="my-input", 
                            value=self.data.initial_value, 
                            type="text"
                        )
                    ]),
                html.Br(),
                html.Div(id="my-output")
            ]
        )
